# Pegaki

## Configuração

Crie um arquivo ```.env``` na raiz do projeto e adicione duas variáveis:

- **pegakiEmail**: email para autenticação
- **pegakiSecret**: client_secret para autenticação

## Serve
Execute o comando ```php spark serve```