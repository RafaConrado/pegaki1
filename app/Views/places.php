<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Consulta locais próximos</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        html,
        body,
        #app {
            height: 100%
        }

        #map {
            height: 100%;
            min-height: 500px;
        }
    </style>
</head>

<body>
    <div id="app">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-md-3">
                    <div class="form-group">
                        <label for="zipCode">CEP (somente números)</label>
                        <input type="text" class="form-control" v-model="zipCode" id="zipCode" aria-describedby="emailHelp">
                    </div>
                    <button type="button" class="btn btn-primary" :disabled="loading" @click.prevent="nearby">
                        <span v-if="loading">Carregando...</span>
                        <span v-else>Consultar</span>
                    </button>
                </div>
                <div class="col-12 col-md-9">
                    <div id="map"></div>
                </div>
            </div>

        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrhnsAUZZb45cYLdJ9ZDh1kCZ5stX09F8&callback=initMap">
    </script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                zipCode: '',
                places: [],
                loading: false
            },
            methods: {
                nearby() {
                    this.loading = true;
                    fetch(`/places/nearby/${this.zipCode}`)
                        .then(response => response.json())
                        .then(data => {
                            if (data.error) {
                                alert(data.message);
                            } else {
                                this.$set(this, 'places', data.results)
                                this.fetchMap();
                            }
                            this.loading = false;
                        });
                },
                fetchMap() {
                    if (this.places && this.places.length) {
                        const map = new google.maps.Map(document.getElementById('map'), {
                            center: {
                                lat: parseFloat(this.places[0].latitude),
                                lng: parseFloat(this.places[0].longitude)
                            },
                            zoom: 10
                        });

                        for (const place of this.places) {
                            new google.maps.Marker({
                                position: {
                                    lat: parseFloat(place.latitude),
                                    lng: parseFloat(place.longitude)
                                },
                                map: map,
                                title: place.nome_fantasia
                            });
                        }
                    } else {
                        const map = new google.maps.Map(document.getElementById('map'), {
                            center: {
                                lat: -25.4950501,
                                lng: -49.4298846
                            },
                            zoom: 8
                        });
                    }

                }
            }
        })

        function initMap() {
            app.fetchMap();
        }
    </script>
</body>

</html>