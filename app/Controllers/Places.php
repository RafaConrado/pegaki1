<?php

namespace App\Controllers;

use App\ThirdParty\pegaki\Authentication;
use App\ThirdParty\pegaki\Places as PlacesApi;

class Places extends BaseController
{
	public function index()
	{
		return view('places');
	}

	public function nearby($zipCode)
	{

		$authApi = new Authentication();

		$authResult = $authApi->auth($_ENV['pegakiEmail'], $_ENV['pegakiSecret']);

		if ($authResult->error)
			return $this->response->setJSON($authResult);

		$placesApi = new PlacesApi($authResult->id_token);

		$nearby = $placesApi->nearby($zipCode);

		return $this->response->setJSON($nearby);
	}

	//--------------------------------------------------------------------

}
