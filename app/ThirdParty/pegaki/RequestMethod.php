<?php

namespace App\ThirdParty\pegaki;

abstract class RequestMethod
{
    const POST = 'POST';
    const PUT = 'PUT';
    const GET = 'GET';
    const DELETE = 'DELETE';
}
