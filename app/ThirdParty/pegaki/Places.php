<?php

namespace App\ThirdParty\pegaki;

class Places extends Api
{

    public function __construct($token)
    {
        $this->accessToken = $token;
    }

    public function nearby($cep)
    {
        return $this->request(RequestMethod::GET, "pontos/$cep");
    }
}
