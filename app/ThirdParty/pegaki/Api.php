<?php

namespace App\ThirdParty\pegaki;

class Api
{
    protected $baseUrl = "https://api.pegaki.com.br/";
    protected $contentType = "application/json";
    protected $accessToken;
    protected $responseCode;

    protected function request($method = RequestMethod::GET, $endpoint, $data = [])
    {
        try {


            if ($method == RequestMethod::GET) {
                $ch = curl_init($this->baseUrl . $endpoint . '?' . http_build_query($data));
                curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: $this->contentType"]);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            } else if ($method == RequestMethod::POST) {
                $ch = curl_init($this->baseUrl . $endpoint);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: $this->contentType"]);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            } else if ($method == RequestMethod::PUT) {
                $ch = curl_init($this->baseUrl . $endpoint);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: $this->contentType"]);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_HEADER, true);
            }

            if ($this->accessToken) curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: $this->accessToken"]);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result        = curl_exec($ch);
            $this->responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $response = json_decode($result);

            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}
