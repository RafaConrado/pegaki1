<?php

namespace App\ThirdParty\pegaki;

class Authentication extends Api
{
    public function auth($email, $client_secret)
    {
        return $this->request(RequestMethod::POST, 'authentication', compact('email', 'client_secret'));
    }
}
